# TAP A220 for MSFS

TAP Designs Group a220 for Microsoft Flight Simulator.
Latest <strong>unstable</strong> features are on the dev branch, latest stable release is in the release section

## Discord
It's where we all talk about the project, but also anything really. Come pls
https://discord.gg/J4DgZeQ

## Contributing
We are not currently looking for contributors. However, if you feel like you can be of value, contact an admin in the Discord server.

## BUG reports / features requests
To report a bug, open a GitLab issue. Include the issue, intended action, and reproduction steps.
You can also contact us on the Discord, an issue will be opened nonetheless.

## Known issues:
- N/A
